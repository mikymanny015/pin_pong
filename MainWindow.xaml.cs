using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
enum dir { left,right,top,botton};
namespace Rachetta_mouse
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool controlla;
        double mousex, mousey;
        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer timer2 = new DispatcherTimer();
        dir direzione = new dir();
        int t =60;
        int trans = 200;
        // per gestire in modo uniforme i retangoli bisonga disegnare e gestire il tutto con dei timer 
        DispatcherTimer disegno = new DispatcherTimer();
        DispatcherTimer gestione_rettangolo = new DispatcherTimer();
        DispatcherTimer Movimento_palla = new DispatcherTimer();
        // variabili punteggio 
      public   int player1 = 0;
        public int player2 = 0;
        bool x = false;
        bool y = false;

        public MainWindow()
        {
            controlla = false;
            //movimento palla 
            Movimento_palla.Interval = TimeSpan.FromMilliseconds(1);
            Movimento_palla.Tick += Movimento_palla_Tick;

            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += Ostacoli_Tick;// controllo movimento del colpo con la racchetta 
            timer2.Interval = TimeSpan.FromMilliseconds(1);
              timer2.Tick += Gestione_palla_Tick;// controllo palla 
            disegno.Interval += TimeSpan.FromMilliseconds(1);
            disegno.Tick += Disegno_Tick;
            gestione_rettangolo.Interval += TimeSpan.FromMilliseconds(1);
            gestione_rettangolo.Tick += Gestione_rettangolo_Tick;


            InitializeComponent();
            WindowState = WindowState.Maximized;// apro la finestra a tutto schermo 
            pq.KeyDown += Pq_KeyDown;
            disegno.Start();
            Movimento_palla.Start();
            timer.Start();
            gestione_rettangolo.Start();
            timer2.Start();
            //timer2.Start();
            mousex = Mouse.GetPosition(myCav).X;
            mousey = Mouse.GetPosition(myCav).Y;


        }

        private void Movimento_palla_Tick(object sender, EventArgs e)
        {
            disegno.Stop();
            int velocità = 10;
            if (x == true)
                Canvas.SetLeft(palla, Canvas.GetLeft(palla) - velocità);//sinistra 
            if (x == false)
                Canvas.SetLeft(palla, Canvas.GetLeft(palla) + velocità);//destra 
            if (y == true)
                Canvas.SetTop(palla, Canvas.GetTop(palla) + 1);// scende
            if (y == false)
                Canvas.SetTop(palla, Canvas.GetTop(palla) - 1);// sale 
        }


        private void Gestione_rettangolo_Tick(object sender, EventArgs e)
        {
            if (Canvas.GetTop(myRect) <= 0)
            {
                Canvas.SetTop(myRect, 1);
            }
            if(Canvas.GetTop(myRect)+myRect.ActualHeight >= ActualHeight)
            {
                Canvas.SetTop(myRect, ActualHeight-myRect.ActualHeight-40);
            }

            if (Canvas.GetTop(racchetta2) <= 0)
            {
                Canvas.SetTop(racchetta2, 1);
            }
            if (Canvas.GetTop(racchetta2) + racchetta2.ActualHeight >= ActualHeight)
            {
                Canvas.SetTop(racchetta2, ActualHeight - racchetta2.ActualHeight - 40);
            }
        }

        private void Disegno_Tick(object sender, EventArgs e)
        {
            Canvas.SetLeft(racchetta2, ActualWidth - racchetta2.Width - 100);
            Canvas.SetTop(racchetta2,((ActualHeight/2)-myRect.Height));
            // label giocaotre 1 
            Canvas.SetTop(p1, 10);
            Canvas.SetLeft(p1, 10);
            // label giocatore 2
            Canvas.SetLeft(p2, ActualWidth - 10-p2.Width);
            Canvas.SetTop(p2, 10);
            // disegno palla 
            Canvas.SetTop(palla, 0);
            Canvas.SetLeft(palla, (ActualWidth / 2));
        }

        private void Pq_KeyDown(object sender, KeyEventArgs e)
        {
            disegno.Stop();
            if (e.Key.Equals(Key.S))
            {
                Canvas.SetTop(myRect, Canvas.GetTop(myRect) + 20);
            }
            if (e.Key.Equals(Key.W))
            {
                Canvas.SetTop(myRect, Canvas.GetTop(myRect) - 20);
            }
            if (e.Key.Equals(Key.Up))
            {
                Canvas.SetTop(racchetta2, Canvas.GetTop(racchetta2) - 20);
            }
            if (e.Key.Equals(Key.Down))
            {
                Canvas.SetTop(racchetta2, Canvas.GetTop(racchetta2) + 20);
            }
            if (e.Key.Equals(Key.B))
            {
               Application.Current.Shutdown();
            }
        }

        private void Gestione_palla_Tick(object sender, EventArgs e)
        {
            disegno.Stop();
            if (Canvas.GetLeft(palla) <= 0)
            {
                x = false;// destra 
            }

            if (Canvas.GetLeft(palla) + palla.ActualWidth >= myCav.ActualWidth)// controllo 
            {
               x = true;// sinistra 
                                //Canvas.SetLeft(palla, Canvas.GetLeft(palla) - 90);// rimbalza 
            }

            if (Canvas.GetTop(palla) <= 0)
            {
                y = true;// scende 
            }
            if (Canvas.GetTop(palla) + palla.ActualHeight >= myCav.ActualHeight)
            {
                y= false;// sale
            }
        }
        /*
        private void Timer_Tick(object sender, EventArgs e)
        {
            // quando ticca bisonga gestire l'andata e il ritorno con il left e top, sul quaderno presente schema di gstione delle rachette, 
            //usare quello e implemate i contorlli. 
            if (direzione.Equals(dir.right))
            {
                // impedire la discesa
                Canvas.SetLeft(palla, Canvas.GetLeft(palla) + 40);
                Canvas.SetTop(palla, Canvas.GetTop(palla) + t);
                if(Canvas.GetLeft(palla)+palla.Width> myCav.ActualWidth)
                {
                    direzione = dir.left;
                    direzione = dir.botton;
                    direzione = dir.top;
                }
                if (Canvas.GetRight(palla) + palla.Height >= myCav.ActualHeight)
                {
                    
                   Canvas.SetTop(palla, Canvas.GetTop(palla) -t);
                    //direzione = dir.top;
                    //t = t * 1;
                }
                //if(Canvas.GetBottom(palla)-palla.Height>=)
                if (Canvas.GetBottom(palla) >=myCav.ActualHeight)
                {
                    Canvas.SetBottom(palla, myCav.ActualHeight);
                    direzione = dir.top;
                    t = t * 1;
                }
            }
            else
            {
                Canvas.SetLeft(palla, Canvas.GetLeft(palla) +t);
                if (Canvas.GetLeft(palla) < 0)
                {
                    direzione = dir.right;
                    //direzione = dir.top;
                }
                if (Canvas.GetLeft(palla)+palla.ActualWidth+10 >= myCav.ActualWidth)
                {
                    activate = true;
                    Canvas.SetLeft(palla, Canvas.GetLeft(palla) - trans); 
                }
            }
            
        }
        */
        //finire il riposizionamento del rettangolo e controllare quando si incontrano, ed eventualmente regolare la velocità 

        private void Ostacoli_Tick(object sender,EventArgs e)
        {
            //Colpo();
           // Anti_rimblazo();
            /*
            if (Canvas.GetLeft(palla) <= 0)
            {
                activate = false;// sinistra 
            }
            */
            if (t <= 0)
            {
                t = 60;
            }
            // player 1
            double c = Canvas.GetTop(myRect)-(palla.Height/2);// calcola la distanza, meno 30 serve per avere il metà margine dell ascisse
            // misura la distanza dal inizo al bottone anche dove non è il bottone  
                //(Canvas.GetTop(palla)<=c+myRect.ActualHeight+(palla.Height / 2)))))
                /*
            if((Canvas.GetLeft(palla)<=Canvas.GetLeft(myRect)+myRect.ActualWidth)&&((Canvas.GetTop(palla)+palla.Height >=Canvas.GetTop(myRect)))&&(Canvas.GetTop(palla) <=Canvas.GetTop(myRect)+myRect.Height+30))
            {
                ///Canvas.SetLeft(palla, Canvas.GetLeft(palla) + 10);// va a sinistra 
              
              x = false;
                t = t - 10;
                //come mettere all' intenro di una palla una scritta 
                SettaContatori("p1");
            }
            */
            if (Canvas.GetLeft(palla) + palla.ActualWidth >= Canvas.GetLeft(myRect) &&(Canvas.GetLeft(palla)<=Canvas.GetLeft(myRect)+myRect.Width) && ((Canvas.GetTop(palla) + palla.Height >= Canvas.GetTop(myRect))) && (Canvas.GetTop(palla) <= Canvas.GetTop(myRect) + myRect.Height))
            {
                SettaContatori("p1");
                Canvas.SetLeft(palla, Canvas.GetLeft(myRect) + myRect.ActualWidth + (10));
                x = false;
                t = t - 10;
                
            }
            // player 2 
          //  double c1 = Canvas.GetTop(racchetta2) - (palla.Height / 2);
            if (((Canvas.GetLeft(palla)+palla.Width)>=Canvas.GetLeft(racchetta2))&&(Canvas.GetLeft(palla) <= Canvas.GetLeft(racchetta2) + racchetta2.Width) &&((Canvas.GetTop(palla)+palla.ActualHeight >= Canvas.GetTop(racchetta2))&&(Canvas.GetTop(palla)+palla.ActualHeight<=(Canvas.GetTop(racchetta2)+racchetta2.Height+30))))
            {
                
                t = t - 10;
                SettaContatori("p2");
                Canvas.SetLeft(palla,Canvas.GetLeft(racchetta2)-palla.Width-20);
                x = true;

            }
            
            // contatore 1 
            if (Canvas.GetLeft(palla) <= (Canvas.GetLeft(p1) + p1.Width) && (Canvas.GetTop(palla) <= (Canvas.GetTop(p1) + p1.Height)))
            {
                /// Gestione_movimenti(false);
                //activate = false;
                x = false;
            }
            if ((Canvas.GetLeft(palla) + palla.Width) >= Canvas.GetLeft(p2) && (Canvas.GetTop(palla) <= (Canvas.GetTop(p2) + p2.Height)))
            {
                //Gestione_movimenti(true);
                // activate = true;
                x = true;

            }
          //  */
        }
        private void SettaContatori(string player)
        {
            if (player == "p1")
            {
                player1 = player1 + 1;
                p1.Content = "Score:"+player1;   
            }
            if (player == "p2")
            {
                player2 = player2 + 1;
                p2.Content= "Score:" +player2;
            }

        }
        /*
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (controlla == true)
            {
              //  L1.Content = e.GetPosition(myCav).X;
                //L2.Content = e.GetPosition(myCav).Y;
              Canvas.SetLeft(myRect, e.GetPosition(myCav).X  - myRect.ActualWidth / 2);
               Canvas.SetTop(myRect, e.GetPosition(myCav).Y - myRect.ActualHeight / 2);
                mousex = Mouse.GetPosition(myCav).X;
               // mousey = Mouse.GetPosition(myCav).Y;
            }
        }

        private void myCav_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            //L1.Content = e.Delta;
            Canvas.SetLeft(myRect, Canvas.GetLeft(myRect) - e.Delta / 120 * myRect.Width / 20);
            myRect.Width = myRect.Width + e.Delta/120* myRect.Width/10;
        }

        private void myCav_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            controlla = !controlla;
            mousex = Mouse.GetPosition(myCav).X;
         //  mousey = Mouse.GetPosition(myCav).Y;
        }
        */
    }
}